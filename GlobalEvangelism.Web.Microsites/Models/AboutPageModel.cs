﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace GlobalEvangelism.Web.Microsites.Models
{
    public class AboutPageModel
    {
        public int AboutPageItemId { get; set; }

        public string TitleImage { get; set; }

        // if TitleImage not present, show TitleHTML
        public string TitleHTML { get; set; }

        public string BackgroundImage { get; set; }

        public string LeftSideHTML { get; set; }

        public string HeaderHTML { get; set; }

        public string InnerHTML { get; set; }

        public string FooterHTML { get; set; }

        public Event Event { get; set; }
    }
}