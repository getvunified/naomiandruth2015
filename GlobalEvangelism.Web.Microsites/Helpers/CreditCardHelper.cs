﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalEvangelism.Web.Microsites.Helpers
{
    public static class CreditCardHelper
    {
        public static List<SelectListItem> GetMonthList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                //list.Add(new SelectListItem() { Text = new DateTime(2000, i, 1).ToString("MMM"), Value = i.ToString("00") });//Display as MMM
                list.Add(new SelectListItem() { Text = i.ToString("00"), Value = i.ToString("00") });
            }
            return list;
        }

        public static List<SelectListItem> GetYearList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            for (int i = DateTime.Now.Year; i < DateTime.Now.Year+10; i++)
            {
                list.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString().Substring(2,2) });
            }
            return list;
        }
    }
}