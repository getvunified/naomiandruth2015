﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GlobalEvangelism.Web.Microsites.Models
{
    public class EventSession : BaseEntity
    {     
        public string Name { get; set; }
        public string Description { get; set; }
        public string EventID { get; set; }
        public string EventName { get; set; }
        public DateTime StartDate { get; set; }     // UTC time
        public DateTime EndDate { get; set; }       // UTC time
        public int UTCOffset { get; set; }          // optional
        public Decimal Price { get; set; }
        public string NotesHTML { get; set; }
        public bool SeatsAvailable { get; set; }
    }
}