﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;
using Microsoft.Win32.SafeHandles;

namespace GlobalEvangelism.Web.Microsites.Models
{
    public class Event : BaseEntity
    {     
        public string Name { get; set; }
        public string Description { get; set; }
        public string EventID { get; set; }
        public string SiteName { get; set; }

        public string CSS { get; set; }                         // Enhances the base site CSS
        public string HeaderHTML { get; set; }
        public string InnerHTML { get; set; }
        public string FooterHTML { get; set; }

        public List<EventSession> Sessions { get; set; }        

        public string EMailFromAccount { get; set; }
        public string EmailToAccount { get; set; }

        // Pre-Registration
        public DateTime PreRegistrationStartDate { get; set; }   // UTC time
        public DateTime PreRegistrationEndDate { get; set; }     // Usually the Same as RegistrationStatDate below, but could be sooner
        public Decimal PreRegistrationPrice { get; set; }
        public string PreRegistrationDescription { get; set; }
        public bool PreRegistrationChecked { get; set; }
       
        // Full Registration
        public DateTime FullRegistrationStartDate { get; set; }     // UTC time
        public DateTime FullRegistrationEndDate { get; set; }       // UTC time
        public Decimal FullRegistrationPrice { get; set; }
        public string FullRegistrationDescription { get; set; }
        public bool FullRegistrationChecked { get; set; }

        public int UTCOffset { get; set; }          
        public string NotesHTML { get; set; }
        public bool SeatsAvailable { get; set; }

        public bool GuestsEnabled { get; set; }
        public int MaxGuests { get; set; }
        public string GuestLabel { get; set; }

        public bool ChildcareEnabled { get; set; }
        public int MaxChildren { get; set; }
        public string ChildcareAgeOptions { get; set; }         // Pipe delimited list of age label and value pairs
   
        public bool SiteRequiresLogin { get; set; }
        public bool AllowJHMLogin { get; set; }
        public bool AllowGoogleLogin { get; set; }
        public bool AllowFacebookLogin { get; set; }
        public bool AllowTwitterLogin { get; set; }
        
    }
}