﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using GlobalEvangelism.Web.Microsites.Helpers;

namespace GlobalEvangelism.Web.Microsites.Models
{
	public abstract class BaseEntity
	{
		[Key]
		[Required]
		public Guid PrimaryKey { get; set; }

		[Required]
		public string Id { get; set; }

		[Required]
		[ScaffoldColumn(false)]
		[DisplayName("Date Created")]
		public DateTime? DateCreated { get; set; }

		[Required]
		[ScaffoldColumn(false)]
		[DisplayName("Date Modified")]
		public DateTime? DateModified { get; set; }

		[Required]
		[ScaffoldColumn(false)]
		[DisplayName("Created By")]
		public string CreatedBy { get; set; }

		[Required]
		[ScaffoldColumn(false)]
		[DisplayName("Modified By")]
		public string ModifiedBy { get; set; }

		[Timestamp]
		public virtual Byte[] TimeStamp { get; set; }

		public bool Initialize()
		{
			this.PrimaryKey = (this.PrimaryKey != Guid.Empty) ? this.PrimaryKey : SequentialGuid.NewGuid();
			this.Id = this.Id ?? CalculateMD5Hash(this.PrimaryKey.ToString("N"));
			this.CreatedBy = this.CreatedBy ?? "system";
			this.ModifiedBy = this.ModifiedBy ?? "system";
			this.DateCreated = this.DateCreated ?? DateTime.UtcNow;
            this.DateModified = this.DateModified ?? DateTime.UtcNow;
			return true;
		}

        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

        [DefaultValue(true)]
        public bool IsActive { get; set; }

	}
}