﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Dynamic;
using System.Globalization;
using System.Web.Security;
using Jhm.Code;

namespace GlobalEvangelism.Web.Microsites.Models
{
    public class Registration: BaseEntity
    {
        [Display(Name = "Is this a Gift?")]
        public bool IsAGift { get; set; }

        [Required(ErrorMessage = "Your name is required.")]
        [Display(Name = "Your Name")]
        public string GiftInfo { get; set; }

        [Required(ErrorMessage = "Your email is required.")]
        [Display(Name = "Your Email")]
        public string GiftEmail { get; set; }
        
        [Required(ErrorMessage="First name required.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name required.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Address line 1 required.")]
        [Display(Name = "Address Line 1")]
        public string Address1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string Address2 { get; set; }

        [Required(ErrorMessage = "City required.")]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "State required.")]
        [Display(Name = "State")]
        public string State { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Postal Code")]
        [Range(0,99999,ErrorMessage="Code format incorrect.")]
        public string PostalCode { get; set; }

        [Required(ErrorMessage = "Required")]
        public string ContactPreference { get; set; }

        [NotMapped]
        public string ContactPreferenceOptions { get; set; }

        [Display(Name = "Work Phone")]
        public string WorkPhone { get; set; }

        [Display(Name = "Home Phone")]
        public string HomePhone { get; set; }

        [Display(Name = "Phone")]
        [Required(ErrorMessage="Phone required.")]
        public string MobilePhone { get; set; }

        public string Fax { get; set; }

        [EmailAddress(ErrorMessage = "Email format incorrect.")]
        [Display(Name = "Email Address")]
        [Required(ErrorMessage = "Email required.")]
        public string Email { get; set; }

        public string Website { get; set; }      

        public string Guests { get; set; }                      // | delimited list of Guests

        public int GuestCount { get; set; }

        public string Children { get; set; }                    // | delimited list of Children 

        public int ChildCount { get; set; }

        public string Notes { get; set; }

        [Required(ErrorMessage="Required")]
        public bool SpecialInstructions { get; set; }           // | Delimited list of boolean special instructions such as "Child Care Required|Text Me The Evening of the Event", etc.
        
        public bool SpecialInstructionsSelected { get; set; }

        public string CouponCode { get; set; }                  // Currently only supports full registration for all sessions

        #region Payment Properties
        [Required(ErrorMessage="Please select a payment type.")]
        public string PaymentType {get;set;} 

        [Required(ErrorMessage="Card Number Required.")]
        [Display(Name="Card Number")]
        public string CreditCardNumber { get; set; }

        [Required(ErrorMessage = "Required.")]
        [Display(Name = "Expiration Date")]
        public string CreditCardExpirationDate { get; set; }

        [Required(ErrorMessage = "Code Required.")]
        [Display(Name = "Security Code")]
        public string CreditCardCID { get; set; }

        [Required(ErrorMessage = "Name Required.")]
        [Display(Name = "Name on Card")]
        public string CreditCardName { get; set; }

        public string CreditCardType { get; set; }

        [Required(ErrorMessage = "Routing No. Required.")]
        [Display(Name = "Routing Number")]
        public string CheckRoutingNumber { get; set; }

        [Required(ErrorMessage = "Account No. Required.")]
        [Display(Name = "Account Number")]
        public string CheckAccountNumber { get; set; }

        [NotMapped]
        public string CheckBankName { get; set; }
        [NotMapped]
        public string CheckBankPhone { get; set; }
        [NotMapped]
        public string CheckBankAddress { get; set; }
        [NotMapped]
        public string CheckBankCity { get; set; }
        [NotMapped]
        public string CheckBankState { get; set; }
        [NotMapped]
        public string CheckBankZip { get; set; }
        #endregion

        public string EventID { get; set; }

        [NotMapped]
        public Event Event { get; set; }

        [NotMapped]
        public List<EventSession> SelectedEventSessions { get; set; }

        public string EventSessionsConcat { get; set; }

        public Decimal AmountDue { get; set; }
        public Decimal AmountPaid { get; set; }
        public Decimal Discount { get; set; }
        public string DiscountReason { get; set; }

        public string TransactionID { get; set; }

        public Boolean IsPaid { get; set; }  
        public string RegistrationCode { get; set; } 
    }

}
