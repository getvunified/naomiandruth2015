﻿$(document).ready(function() {
    //display giftcard information input
    $('#Registration_IsAGift').on('click', function () {
        if (this.checked) {
            $('.GiftInfo').slideDown('slow');
            $('.GiftField').show();
            $('.NotGiftField').hide();
        } else {
            $('.GiftInfo').slideUp('slow');
            $('.GiftField').hide();
            $('.NotGiftField').show();
        }
    });

    $("form").data("validator").settings.ignore = ".data-val-ignore, :hidden, :disabled";

    $('#CreditCardFields').change(function () {
        $('#PaymentErrorMessage').hide();
    });
    $('#CheckFields').change(function () {
        $('#PaymentErrorMessage').hide();
    });
});

function ShowPanel(panelName, html, onSuccess) {
    if (html != undefined && html != null) {
        $('#' + panelName + 'Panel').html(html);
    }
    if (!$('#' + panelName + 'Panel').is(':visible')) {
        $('.PagePanel:visible').hide("slide", function () {
            $('#' + panelName + 'Panel').show("slide", onSuccess);
        });
    }
}

function ContactPrefChanged(type)
{
    if (type == 'Email') {
        $("#Registration_Email").removeClass('data-val-ignore');
        $("#Registration_Email").valid();
        $("#Registration_MobilePhone").addClass('data-val-ignore');
        $("#Registration_MobilePhone").removeClass('input-validation-error').addClass('input-validation-valid');
        $("#Registration_MobilePhone").prev().prev().removeClass('field-validation-error').addClass('field-validation-valid');
    }
    else {
        $("#Registration_MobilePhone").removeClass('data-val-ignore');
        $("#Registration_MobilePhone").valid();
        $("#Registration_Email").addClass('data-val-ignore');
        $("#Registration_Email").removeClass('input-validation-error').addClass('input-validation-valid');
        $("#Registration_Email").prev().prev().removeClass('field-validation-error').addClass('field-validation-valid');
    }
}

function ShowChildrenInfoPnl()
{
    $('.ChildrenInfoPnl').slideDown('slow');
}

function HideChildrenInfoPnl()
{
    $('.ChildrenInfoPnl').slideUp('slow');
}

function AddChild()
{
    var maxChildren = parseInt($('#Registration_Event_MaxChildren').val());

    if ($('.ChildrenList').children().length < maxChildren) {
        //Adds a new ChildInfo class div
        var childInfo = $('.ChildrenList .ChildInfo').first().clone();
        $('.ChildrenList').append(childInfo);
        $('.ChildrenList .ChildInfo').last().find('.ChildName').val('');
        $('.ChildrenList .ChildInfo').last().find('.ChildAge').val('');
        $('.ChildrenList .ChildInfo').last().find('.input-validation-error').removeClass('input-validation-error').addClass('input-validation-valid');
        $('.ChildrenList .ChildInfo').last().find('.field-validation-error').removeClass('field-validation-error').addClass('field-validation-valid');

        if ($('.ChildrenList').children().length === maxChildren) {
            $('#AddChildBtn').hide();
        }
    }
}

function RemoveChild(sender)
{
    var maxChildren = parseInt($('#Registration_Event_MaxChildren').val());

    //Removes ChildInfo
    if ($('.ChildrenList').children().length === 1) {
        $('.ChildrenList .ChildInfo').last().find('.ChildName').val('');
        $('.ChildrenList .ChildInfo').last().find('.ChildAge').val('');
        $('#Registration_SpecialInstructions').prop('checked', true);
        HideChildrenInfoPnl();
    }
    else {
        $(sender).closest('.ChildInfo').remove();

        if ($('.ChildrenList').children().length < maxChildren) {
            $('#AddChildBtn').show();
        }
    }
}

function SetChildrenList()
{
    var children = "";
    $('.ChildInfo').each(function () {
        var childName = $(this).find('.ChildName').val();
        if (childName != undefined && childName != null && childName.trim() != '') {
            var childAge = $(this).find('.ChildAge').val();
            if (childAge != 'Select Age') {
                children = children + childName + ":" + childAge + "|";
                $(this).find('.ChildAge').removeClass('input-validation-error');
                $(this).find('.field-validation-error').hide();
            }
            else {
                $(this).find('.ChildAge').addClass('input-validation-error');
                $(this).find('.field-validation-error').show();
            }
        }
    });
    children = children.slice(0, -1);
    $('#Registration_Children').val(children);
}

function RegisterAllChanged()
{
    var checked = $('.RegisterAllBtn').is(':checked');
    $('#EventTable input[type=checkbox]').each(function () {
        this.checked = checked;
    });

    UpdateAmountDue();
}

function RegisterEventChanged()
{
    if ($('.RegisterEventBtn').length === $('.RegisterEventBtn:checked').length) {//All are checked
        $('.RegisterAllBtn').prop("checked", true);
    }
    else {
        $('.RegisterAllBtn').prop("checked", false);
    }

    UpdateAmountDue();
}

function UpdateAmountDue()
{
    var price = 0.00;
    var bundlePrice = parseFloat($('.RegisterAllBtn').data('price'));
    if ($('.RegisterAllBtn').is(':checked')) {//Use super saver amount
        price = bundlePrice;
    }
    else {
        $('.RegisterEventBtn:checked').each(function () {
            price += parseFloat($(this).data('price'));
        })
    }

    if (bundlePrice < price) {
        $('#SaveMessage').fadeIn('slow');
    }
    else {
        $('#SaveMessage').fadeOut('slow');
    }

    $('.reg-amt').html(price.toFixed(2));
    $('#Registration_AmountDue').val(price);

    //Check if no sessions are selected and disabled sign up button until they have at least one selected
    if (price == 0.00) {
        $('#SignUpBtn').attr('disabled', 'disabled');
        $('#SessionsPnlValidationMsg').show();
        $('#SessionsPnl').addClass('input-validation-error');
    }
    else {
        $('#SignUpBtn').removeAttr('disabled');
        $('#SessionsPnlValidationMsg').hide();
        $('#SessionsPnl').removeClass('input-validation-error');
    }

    RegisterSetSession();
}

function SelectAllEvents()
{
    $('.RegisterAllBtn').prop("checked", true);
    RegisterAllChanged();
}

function RegisterSetSession()
{
    var sessionValue = "";
    $('.RegisterEventBtn:checked').each(function () {
        sessionValue += $(this).prop('id') + '|';
    })
    sessionValue = sessionValue.slice(0, -1);
    $('#Registration_EventSessionsConcat').val(sessionValue);
}

function PaymentTypeChanged(type)
{
    switch(type)
    {
        case 'Check':
            $('#CreditCardFields').slideUp('slow', function() {
                $('#CheckFields').slideDown('slow');
            });
            $("#CreditCardFields input").addClass('data-val-ignore');
            $("#CheckFields input").removeClass('data-val-ignore');
            break;
        case 'CreditCard':
            $('#CheckFields').slideUp('slow', function() {
                $('#CreditCardFields').slideDown('slow');
            });
            $("#CheckFields input").addClass('data-val-ignore');
            $("#CreditCardFields input").removeClass('data-val-ignore');
            break;
    }
    //$('#Registration_PaymentType').val(type);
    $('#PaymentType').val(type);
}

function CheckChanged()
{
    //Verify aba's match and accound is provided
    var routing1 = $('#Registration_CheckRoutingNumber').val();
    var routing2 = $('#Registration_CheckRoutingNumberConfirm').val();
    var account = $('#Registration_CheckAccountNumber').val();

    //Clear previous errors
    $('#CheckingGeneralError').hide();

    if (routing1 === undefined || routing1 === '' || routing2 === undefined || routing2 === '' || account === undefined || account === '') {
        $('#CheckingGeneralError').hide();
        $('#BankName').closest('.col-md-6').hide();
        if (routing1 != undefined || routing1 != '') {
            $('#Registration_CheckRoutingNumberConfirmValidationMsg').show();
            $('#Registration_CheckRoutingNumberConfirm').addClass('input-validation-error');
        }
        return;
    }

    //Clear confirm validation errors if we get to this point
    $('#Registration_CheckRoutingNumberConfirmValidationMsg').hide();
    $('#Registration_CheckRoutingNumberConfirm').removeClass('input-validation-error');

    if (routing1 != routing2) {
        $('#CheckingGeneralError').html('Routing numbers do not match.');
        $('#CheckingGeneralError').show();
        $('#BankName').closest('.col-md-6').hide();
        return;
    }

    //run lookup, if true, enable Sign In button, if false, display bank not found error message and empty the "re-enter routing" field
    LookupBankInfo(routing1, function (success) {
        if (success) {
            $('#BankName').html($('#Registration_CheckBankName').val());
            $('#BankName').closest('.col-md-6').show();
        }
        else {
            $('#CheckingGeneralError').html('Bank not found. Please check your routing number.');
            $('#CheckingGeneralError').show();
            $('#BankName').closest('.col-md-6').hide();
            return;
        }
    });
}

function LookupBankInfo(routingNumber, f)
{
    $.ajax({
        //url: 'http://www.routingnumbers.info/api/data.json',//Non-SSL
        url: 'https://routingnumbers.herokuapp.com/api/data.json',//SSL
        type: "POST",
        dataType: 'jsonp',
        data: {
            rn: routingNumber
        },
        success: function(data) {
            if (data.code === 400)
            {
                if (typeof f == "function") {
                    f(false);
                }
                return;
            }
            if (data.record_type_code === 0) {
                return LookupBankInfo(data.new_routing_number);
            }
            else {
                $('#Registration_CheckBankName').val(data.customer_name);
                $('#Registration_CheckBankPhone').val(data.telephone);
                $('#Registration_CheckBankAddress').val(data.address);
                $('#Registration_CheckBankCity').val(data.city);
                $('#Registration_CheckBankState').val(data.state);
                $('#Registration_CheckBankZip').val(data.zip);

                if (typeof f == "function") {
                    f(true);
                }
            }
        },
        error: function (xhr, status, error) {
            var error = eval("(" + xhr.responseText + ")");
            alert(error.Message);
        }
    })
}

function CreditCardNameChanged()
{
    var first = $('#Registration_CreditCardFirstName').val();
    var last = $('#Registration_CreditCardLastName').val();

    //If either name fields already has a | in it, clear name field to prevent them from submitting an invalid name that will cause issues
    if (first === undefined || first === '' || first.indexOf('|') != -1 || last === undefined || last === '' || last.indexOf('|') != -1) {
        $('#Registration_CreditCardName').val('');
        if (first.indexOf('|') != -1) {
            $('#Registration_CreditCardFirstName').val('');
        }
        if (last.indexOf('|') != -1) {
            $('#Registration_CreditCardLastName').val('');
        }
        return;
    }
    var ccName = $('#Registration_CreditCardName');
    var ccNameText = first + '|' + last;
    $('#Registration_CreditCardName').val(first + '|' + last);

    if ($('.field-validation-error[data-valmsg-for="Registration.CreditCardName"]')) {
        $('#Registration_CreditCardName').valid();
    }
}

function CreditCardNumberChanged()
{
    if (CheckCardNumber($('#Registration_CreditCardNumber').val())) {
        $('#Registration_CreditCardNumber').addClass('ValidCreditCard');
    }
    else {
        $('#Registration_CreditCardNumber').removeClass('ValidCreditCard');
    }

    //Clear manually error
    $("#Registration_CreditCardNumber").removeClass('input-validation-error').addClass('input-validation-valid');
    $("#CreditCardInvalidMsg").hide();
}

function CreditCardExpirationChanged()
{
    var month = $('#Registration_CreditCardExpirationMonth').val();
    var year = $('#Registration_CreditCardExpirationYear').val();
    if(month != undefined && month != '' && month != 'MM' && year != undefined && year != '' && year != 'YY')
    {
        $('#Registration_CreditCardExpirationDate').val(month + '/' + year);

        if($('.field-validation-error[data-valmsg-for="Registration.CreditCardExpirationDate"]'))
        {
            $('#Registration_CreditCardExpirationDate').valid();
        }
    }
    else {
        $('#Registration_CreditCardExpirationDate').val('');
    }
}

function CheckCardNumber(input) {
    var type = "";
    var typeHtml = "";
    var isValid = false;
    if (input.substring(0, 2) == "34" || input.substring(0, 2) == "37") {
        type = "Amex";
        typeHtml = "(Amex) ";
        if (input.length === 15) {
            isValid = CheckLuhn(input);
        }
    }
    else if (input.substring(0, 2) == "60" || input.substring(0, 2) == "62" || input.substring(0, 2) == "64" || input.substring(0, 2) == "65") {
        type = "Discover";
        typeHtml = "(Discover) ";
        if (input.length === 16) {
            isValid = CheckLuhn(input);
        }
    }
    else if (input.substring(0, 2) == "51" || input.substring(0, 2) == "52" || input.substring(0, 2) == "53" || input.substring(0, 2) == "54" || input.substring(0, 2) == "55") {
        type = "Mastercard";
        typeHtml = "(Mastercard) ";
        if (input.length === 16) {
            isValid = CheckLuhn(input);
        }
    }
    else if (input.substring(0, 1) == "4") {
        type = "Visa";
        typeHtml = "(Visa) ";
        if (input.length === 13 || input.length === 16) {
            isValid = CheckLuhn(input);
        }
    }
    else {
        type = "&nbsp;";
    }

    $('#Registration_CreditCardType').val(type);
    if ($('#RegistrationCreditCardType').html() != typeHtml) {
        $('#RegistrationCreditCardType').fadeOut('slow', function() {
            $(this).html(typeHtml).fadeIn('slow');
        });
    }

    return isValid;
}

function CheckLuhn(input) {
    var sum = 0;
    var numdigits = input.length;
    var parity = numdigits % 2;
    for (var i = 0; i < numdigits; i++) {
        var digit = parseInt(input.charAt(i))
        if (i % 2 == parity) { digit *= 2; }
        if (digit > 9) { digit -= 9; }
        sum += digit;
    }
    return (sum % 10) == 0;
}

function SignUp()
{
    SetChildrenList();
    CreditCardNameChanged();
    CreditCardExpirationChanged();

    //Check if credit card was valid or not
    var isCC = $('#Registration_PaymentType').val() === "CreditCard";// $('#CreditCard').is(':checked');
    var cardNum = $('#Registration_CreditCardNumber').val();
    var isCCValid = CheckCardNumber(cardNum);

    if ($('#RegisterForm').valid() && $('.ChildrenList .field-validation-error:visible').length == 0 && (!isCC || isCCValid))
    {
        var postData = $('#RegisterForm').serializeArray();
        var formURL = $('#RegisterForm').attr("action");
        $.ajax(
        {
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data) 
            {
                ShowPanel('Summary', data);
            }
        });
    }
    else {
        //Manually set credit card error if validation failed
        if (isCC && !isCCValid && cardNum != '') {
            $("#Registration_CreditCardNumber").addClass('input-validation-error').removeClass('input-validation-valid');
            $("#CreditCardInvalidMsg").show();
        }
    }
}

function SubmitRegistration() {
    if ($('#SummaryForm').valid()) {
        var postData = $('#SummaryForm').serializeArray();
        var formURL = $('#SummaryForm').attr("action");
        $.ajax(
        {
            url: formURL,
            type: "POST",
            data: postData,
            success: function (data) {
                if (data.registrationCode != undefined) {
                    $('#RegistrationSuccessMessage').html(data.successMessage);
                    $('#RegistrationSuccessMessage').show();
                    window.scrollTo(0, 0);
                    $('input').attr('disabled', 'disabled');
                    $('#SummaryBtnsPnl').hide();
                }
                else {
                    if (data.errorCatagory === "Registration") {
                        $('#RegistrationErrorMessage').html(data.errorMessage);
                        $('#RegistrationErrorMessage').show();
                        window.scrollTo(0, 0);
                        $('input').attr('disabled', 'disabled');
                    }
                    else {
                        $('#PaymentErrorMessage').html(data.errorMessage);
                        $('#PaymentErrorMessage').show();
                        scrollY = document.body.scrollHeight;
                        ShowPanel('Register', null, function () {
                            window.scrollTo(0, scrollY);
                        });
                    }
                }
            }
        });
    }
}