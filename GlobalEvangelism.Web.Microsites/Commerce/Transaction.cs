﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Code
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public decimal TotalTransaction { get; set; }
        public decimal AdditionalDonation { get; set; }
        public string TransactionId { get; set; }
    }
}
