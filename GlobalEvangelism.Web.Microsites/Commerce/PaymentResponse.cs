
namespace CommerceIntegration
{
    public class PaymentResponse : IPaymentResponse
    {
        private bool isAuthorized;
        private string reasonText;
        private string approvalCode;
        private string transactionID;

        public PaymentResponse(bool isAuthorized, string reasonText, string approvalCode, string transactionID)
        {
            this.isAuthorized = isAuthorized;
            this.transactionID = transactionID;
            this.approvalCode = approvalCode;
            this.reasonText = reasonText;
        }

        public bool IsAuthorized
        {
            get { return isAuthorized; }
        }

        public string ReasonText
        {
            get { return reasonText; }
        }

        public string ApprovalCode
        {
            get { return approvalCode; }
        }

        public string TransactionId
        {
            get { return transactionID;  }
        }

        public string TransactionID
        {
            get { return transactionID; }
        }
    }
}