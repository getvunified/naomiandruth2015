﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.UI.WebControls;
using GlobalEvangelism.Web.Microsites;
using GlobalEvangelism.Web.Microsites.Models;
using DotNetOpenAuth.OpenId.Extensions.AttributeExchange;

namespace Jhm.Code
{
    public class EmailService
    {
        public string EmailFromAccount { get; set; } 
        private static readonly Emailer _emailer = new Emailer();
        MicrositeContext db = new MicrositeContext();

        public MailMessage CreateClientEmail(Registration registration, CommerceIntegration.IPaymentResponse response, string templatePath)
        {
            try
            {
                int i = 1;
                var body = System.IO.File.ReadAllText(templatePath);

                EmailFromAccount = registration.Event.EMailFromAccount;

                string AccountNumber = registration.CreditCardNumber;
                if (registration.PaymentType.ToUpper() == "Check".ToUpper())
                {
                    int len = registration.CheckAccountNumber.Length;
                    if (len > 4)
                    {
                        AccountNumber = registration.CheckAccountNumber.Substring(len - 4, 4);
                    }
                }

                // prep items
                string itemdetail = String.Empty;
                List<string> items = new List<string>();
                String eventName = String.Empty;
                String sessionslist = registration.Event.Name ?? String.Empty;
                if (registration.EventSessionsConcat != null)
                {
                    foreach (string eventsessionId in registration.EventSessionsConcat.Split('|'))
                    {
                        EventSession eve = db.EventSessions.FirstOrDefault(e => e.Id == eventsessionId);
                        if (eve != null)
                        {
                            itemdetail = String.Format("{0} - {1}", eve.StartDate.ToShortDateString(), eve.Name);
                            items.Add(itemdetail);
                            eventName = eve.EventName;
                        }
                    }
                    sessionslist = "<br>" + String.Join("<br><br>", items);
                }
                else
                {
                    var eSessions = db.EventSessions.Where(e => e.EventID == registration.EventID);
                    foreach (EventSession eve in eSessions)
                    {
                        itemdetail = String.Format("{0} - {1}", eve.StartDate.ToShortDateString(), eve.Name);
                        items.Add(itemdetail);
                        eventName = eve.EventName;
                    }
                    sessionslist = "<br>" + String.Join("<br><br>", items);
                }

                string GiftInfo = string.Empty;
                if (registration.IsAGift)
                {
                    GiftInfo = String.Format("Purchased by : {0}", registration.GiftInfo);
                }

                string concatAddress = registration.Address1;
                if (registration.Address2 != null || registration.Address1 != null)
                {
                    concatAddress = String.Format("{0}<br>{1}", registration.Address1, registration.Address2);
                }

                var replacements = new ListDictionary
                    {
                        {"<%PurchasedOnBehalf%>", GiftInfo},
                        {"<%RegistrationCode%>", registration.RegistrationCode ?? String.Empty},
                        {"<%FirstName%>", registration.FirstName ?? String.Empty},
                        {"<%LastName%>", registration.LastName ?? String.Empty},
                        {"<%Email%>", registration.Email ?? String.Empty},
                        {"<%Phone%>", registration.HomePhone ?? String.Empty},
                        {"<%Address%>",concatAddress},
                        {"<%City%>", registration.City ?? String.Empty},
                        {"<%State%>", registration.State ?? String.Empty},
                        {"<%Zip%>", registration.PostalCode ?? String.Empty},
                        {"<%TotalCharge%>", registration.AmountPaid.ToString("C")},
                        {"<%PaymentMethod%>", registration.PaymentType ?? String.Empty},
                        {"<%AccountNumber%>", AccountNumber},
                        {"<%NumberOfItems%>", items.Count().ToString()},
                        {"<%Items%>", sessionslist},
                        {"<%TransactionNumber%>", response.TransactionId ?? String.Empty}                 
                    };

                var md = new MailDefinition
                {
                    From = EmailFromAccount,
                    IsBodyHtml = true,
                    Subject = String.Format("Registration Receipt : {0} ", eventName)
                };

                var msg = md.CreateMailMessage(registration.Email, replacements, body, new System.Web.UI.Control());
                return msg;
            }
            catch (Exception ex)
            {
                return null;
            }
           
        }    

        public void SendEmail(MailMessage message, string toEmailAddress)
        {
            var msg = new MailMessage(message.From, new MailAddress(toEmailAddress))
            {
                IsBodyHtml = true,
                Subject = message.Subject,
                Body = message.Body
            };
            _emailer.SendEmail(msg);
        }

        public void SendClientEmail(Registration registration, CommerceIntegration.IPaymentResponse response, string templatePath)
        {
            var msg = CreateClientEmail(registration, response, templatePath);
            _emailer.SendEmail(msg);
        }

        
    }
}
