﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Jhm.Code
{
    public class FileService
    {
        public static void SaveReceiptEmail(string content,string rootDirectoryPath, string transactionId)
        {
            if ( !Directory.Exists(rootDirectoryPath) )
            {
                Directory.CreateDirectory(rootDirectoryPath);
            }
            var fileName = String.Format("{0:yyyy-MM-dd_HH_mm_ss} {1}.htm", DateTime.Now, String.IsNullOrEmpty(transactionId) ? "(no transaction id)" : transactionId);
            File.WriteAllText(rootDirectoryPath + "\\" + fileName, content);
        }
    }
}
