
namespace CommerceIntegration
{
    public interface IPaymentService
    {
        IPaymentResponse Process(IPaymentRequest request);
    }
}
