
namespace CommerceIntegration
{
    public interface IPaymentResponse
    {
        bool IsAuthorized { get;  }
        string ReasonText { get; }
        string ApprovalCode { get; }
        string TransactionId { get; }
    }
}