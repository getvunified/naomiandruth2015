﻿using System;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace Jhm.Code
{
    //http://weblogs.asp.net/scottgu/archive/2005/12/10/432854.aspx
    //     <system.net>
    //    <mailSettings>
    //      <smtp from="test@foo.com">
    //        <network host="smtpserver1" port="25" userName="username" password="secret" defaultCredentials="true" />
    //      </smtp>
    //    </mailSettings>
    //  </system.net>

    public class Emailer
    {
        public Emailer() { }

        public void SendEmail(MailMessage message)
        {
            ValidateMailMessage(message);

            try
            {
                //http://aspalliance.com/867_Sending_SMTP_Authenticated_Email_Using_ASPNET_20
                //NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential("username", "password");
                ////Put your own, or your ISPs, mail server name onthis next line
                //mailClient.Host = "Mail.RemoteMailServer.com";
                //mailClient.UseDefaultCredentials = false;
                //mailClient.Credentials = basicAuthenticationInfo;
                //mailClient.UseDefaultCredentials = true;
                //mailClient.EnableSsl = true;

                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                var sendEx = new Exception("Failed to send email", ex);
                //Log.For(this).Erroneous(sendEx);
                throw sendEx;
            }
        }

        private static void ValidateMailMessage(MailMessage message)
        {
            if (message.From == null || message.From.Address == string.Empty)
            {
                throw new Exception("Cannot send email. From address is required.");
            }
            if (message.To == null || message.To.Count == 0)
            {
                throw new Exception("Cannot send email. At least one To address is required.");
            }
            if (!IsValidEmail(message.From.Address))
            {
                throw new Exception("Cannot send email. From address is invalid.");
            }
            if (!IsValidEmailCollection(message.To))
            {
                throw new Exception("Cannot send email. To contains an invalid email address.");
            }
            if (!IsValidEmailCollection(message.CC))
            {
                throw new Exception("Cannot send email. CC contains an invalid email address.");
            }
            if (!IsValidEmailCollection(message.Bcc))
            {
                throw new Exception("Cannot send email. BCC contains an invalid email address.");
            }
        }
        private static MailMessage CreateMailMessage(string to, string subject, string body, params Attachment[] attachments)
        {
            return CreateMailMessage(null, to, null, null, subject, body, false, attachments);
        }
        private static MailMessage CreateMailMessage(string from, string to, string subject, string body, params Attachment[] attachments)
        {
            return CreateMailMessage(from, to, null, null, subject, body, false, attachments);
        }
        private static MailMessage CreateMailMessage(string from, string to, string subject, string body, bool isBodyHtml, params Attachment[] attachments)
        {
            return CreateMailMessage(from, to, null, null, subject, body, isBodyHtml, attachments);
        }
        private static MailMessage CreateMailMessage(string from, string to, string cc, string bcc, string subject, string body, bool isBodyHtml, params Attachment[] attachments)
        {
            MailMessage message = new MailMessage();

            if (!string.IsNullOrEmpty(from))
            {
                if (!IsValidEmail(from))
                {
                    throw new Exception("Cannot create email. From address is invalid.");
                }
                message.From = new MailAddress(from, from);
                message.ReplyToList.Add(message.From);
            }
            if (!string.IsNullOrEmpty(to))
            {
                if (!IsValidEmailList(to))
                {
                    throw new Exception("Cannot create email. From address is invalid.");
                }
                message.To.Add(to.Trim().Replace(';', ','));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                if (!IsValidEmailList(cc))
                {
                    throw new Exception("Cannot create email. From address is invalid.");
                }
                message.CC.Add(cc.Trim().Replace(';', ','));
            }
            if (!string.IsNullOrEmpty(bcc))
            {
                if (!IsValidEmailList(cc))
                {
                    throw new Exception("Cannot create email. From address is invalid.");
                }
                message.Bcc.Add(bcc.Trim().Replace(';', ','));
            }
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = isBodyHtml;
            if (attachments != null)
            {
                foreach (Attachment att in attachments)
                {
                    message.Attachments.Add(att);
                }
            }
            return message;
        }
        private static bool IsValidEmailCollection(MailAddressCollection addresses)
        {
            if (addresses == null) return false;

            foreach (MailAddress address in addresses)
            {
                if (address.Address == null || !IsValidEmail(address.Address.Trim()))
                {
                    return false;
                }
            }
            return true;
        }
        private static bool IsValidEmailList(string addresses)
        {
            if (addresses == null) return false;

            string[] emailAddresses = addresses.Split(new char[] { ';', ',' },
                StringSplitOptions.RemoveEmptyEntries);

            foreach (string address in emailAddresses)
            {
                if (!IsValidEmail(address.Trim()))
                {
                    return false;
                }
            }
            return true;
        }
        private static bool IsValidEmail(string emailAddress)
        {
            if (emailAddress == null)
            {
                return false;
            }
            //@"^[a-zA-Z]+(([\'\,\.\- ][a-zA-Z ])?[a-zA-Z]*)*\s+<(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})>$|^(\w[-._\w]*\w@\w[-._\w]*\w\.\w{2,3})$"
            return Regex.IsMatch(emailAddress,
                @"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@(([0-9a-zA-Z])+([-\w]*[0-9a-zA-Z])*\.)+[a-zA-Z]{2,9})$");
        }
        private static Attachment[] TransformAttachments(byte[][] attachments)
        {
            Attachment[] atts = new Attachment[attachments.Length];
            for (int i = 0; i < attachments.Length; i++)
            {
                atts[i] = new Attachment(new MemoryStream(attachments[i]),
                    string.Format("attachment{0}", i + 1));
            }
            return atts;
        }
        private static Attachment[] TransformAttachments(Stream[] attachments)
        {
            Attachment[] atts = new Attachment[attachments.Length];
            for (int i = 0; i < attachments.Length; i++)
            {
                atts[i] = new Attachment(
                    attachments[i], string.Format("attachment{0}", i + 1));
            }
            return atts;
        }
    }
}
