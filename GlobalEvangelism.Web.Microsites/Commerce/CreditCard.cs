﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Jhm.Code
{
    public class CreditCard
    {
        public enum CreditCardType
        {
            [Display(Name = "Visa")]
            Visa,

            [Display(Name = "Master Card")]
            MasterCard,

            [Display(Name = "Discover")]
            Discover,

            [Display(Name = "American Express")]
            Amex
        }
        [NotMapped]
        public CreditCardType CardType { get; set; }
        public int CreditCardTypeId
        {
            get { return (int) CardType; }
            set { CardType = (CreditCardType) value; }
        }
        public string CardNumber { get; set; }
        public string VerificationNumber { get; set; }
        public string NameOnCard { get; set; }
        public int ExpirationMonth { get; set; }
        public int ExpirationYear { get; set; }
        public string BillingZip { get; set; }
        public Guid CreditCardId { get; set; }
        public Guid Id { get; set; }


        public static CreditCardType ParseCreditCardType(string value)
        {
            return (CreditCardType) Enum.Parse(typeof (CreditCardType), value);
        }
    }
}
