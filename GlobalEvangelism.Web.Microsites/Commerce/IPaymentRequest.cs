
namespace CommerceIntegration
{
    public interface IPaymentRequest
    {
        string LoginID { get; }
        string TransactionKey { get; }
        bool IsTest { get; }

        string FirstName { get; }
        string LastName { get; }

        string Address { get; }
        string City { get; }
        string State { get; }
        string Zip { get; }
        string Country { get; }

        string DescriptionOfCharge { get; }
        string CardNumber { get;  }
        string CardExpDate { get; }
        string CardCCV { get;  }
        decimal Amount { get; }
        string Currency { get; set; }
        string InvoiceNumber { get; set; }

    }
}