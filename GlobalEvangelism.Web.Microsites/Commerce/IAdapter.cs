﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Jhm.Code
{
    public interface IAdapter<From, To>
    {
        To CreateFrom(From from);
    }
}
