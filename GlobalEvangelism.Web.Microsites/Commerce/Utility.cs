﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;
using Jhm.Code;

namespace Utility
{
    public class Utility
    { 

        public static int TryParseInt(string value)
        {
            int result;
            int.TryParse(value, out result);
            return result;
        }

        public static long TryParseLong(string value)
        {
            long result;
            long.TryParse(value, out result);
            return result;
        }

        public static float TryParseFloat(string value)
        {
            float result;
            float.TryParse(value, out result);
            return result;
        }

        public static double TryParseDouble(string value)
        {
            double result;
            double.TryParse(value, out result);
            return result;
        }

        public static decimal TryParseDecimal(string value)
        {
            decimal result;
            decimal.TryParse(value, out result);
            return result;
        }

        public static bool IsValidCreditCard(CreditCard cc, out string reason)
        {
            reason = String.Empty;
            if (!NumberIsValid(cc.CardNumber))
            {
                reason = "Credit card number is invalid";

                //FIX THIS - FOR TESTNG ONLY
                return true;
            }
            if (!CardTypeIsValid(cc.CardType))
            {
                reason = "Card type is not supported " + cc.CardType;
                return false;
            }

            if (IsExpired(DateTime.Now, cc.ExpirationMonth, cc.ExpirationYear))
            {
                reason = "Credit Card is expired";
                return false;
            }
            return true;
        }

        private static bool IsExpired(DateTime currentDate, int expirationMonth, int expirationYear)
        {
            return currentDate.Year > expirationYear ||
                   (currentDate.Year == expirationYear && currentDate.Month > expirationMonth);
        }

        private static bool CardTypeIsValid(object cardType)
        {
            return Enum.IsDefined(typeof(CreditCard.CreditCardType),cardType);
        }

        private static bool NumberIsValid(string cardNumber)
        {
            if (String.IsNullOrEmpty(cardNumber)) return false;

            var sum = cardNumber.Where(c => Char.IsDigit(c))
                        .Reverse()
                        .SelectMany((c, i) => ((c - '0') << (i & 1)).ToString())
                        .Sum(c => c - '0') % 10;
            return sum == 0;
        }

        
    }
}
