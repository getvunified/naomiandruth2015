

namespace CommerceIntegration
{
    public class PaymentRequest : IPaymentRequest
    {
        private readonly string loginID;
        private readonly string transactionKey;
        private readonly string firstName;
        private readonly string lastName;
        private readonly string address;
        private readonly string city;
        private readonly string state;
        private readonly string zip;
        private readonly string country;
        private readonly string descriptionOfCharge;
        private readonly string cardNumber;
        private readonly string cardExpDate;
        private readonly string cardCCV;
        private readonly decimal amount;
        private readonly bool isTest;
        

        public PaymentRequest(string loginID, string transactionKey, bool isTest, 
            string firstName, string lastName, 
            string address, string city, string state, string zip, string country,
            string descriptionOfCharge, string cardNumber, string cardExpDate, string cardCCV, decimal amount, string invoiceNumber)
        {
            this.loginID = loginID;
            this.isTest = isTest;
            this.amount = amount;
            this.cardCCV = cardCCV;
            this.cardExpDate = cardExpDate;
            this.cardNumber = cardNumber;
            this.descriptionOfCharge = descriptionOfCharge;
            this.InvoiceNumber = invoiceNumber;
            this.country = country;
            this.zip = zip;
            this.state = state;
            this.city = city;
            this.address = address;
            this.lastName = lastName;
            this.firstName = firstName;
            this.transactionKey = transactionKey;
        }

        public string LoginID
        {
            get { return loginID; }
        }

        public string TransactionKey
        {
            get { return transactionKey; }
        }

        public bool IsTest
        {
            get { return isTest; }
        }

        public string FirstName
        {
            get { return firstName; }
        }

        public string LastName
        {
            get { return lastName; }
        }

        public string Address
        {
            get { return address; }
        }

        public string City
        {
            get { return city; }
        }

        public string State
        {
            get { return state; }
        }

        public string Zip
        {
            get { return zip; }
        }

        public string Country
        {
            get { return country; }
        }

        public string DescriptionOfCharge
        {
            get { return descriptionOfCharge; }
        }

        public string CardNumber
        {
            get { return cardNumber; }
        }

        public string CardExpDate
        {
            get { return cardExpDate; }
        }

        public string CardCCV
        {
            get { return cardCCV; }
        }

        public decimal Amount
        {
            get { return amount; }
        }

        public string Currency { get; set; }

        public string InvoiceNumber { get; set; }
    }
}