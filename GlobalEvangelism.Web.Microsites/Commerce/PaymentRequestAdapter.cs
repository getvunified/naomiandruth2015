﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Jhm.CommerceIntegration;

namespace Jhm.Code
{
    public class PaymentRequestAdapter : IAdapter<Registration, IPaymentRequest>
    {

        private static readonly CommerceAccount CommerceAccount = new CommerceAccount
        {
 
      
            //TODO:  Not working.  Need to check codes

             //"22r8w9LKq", "2H2K3v8cVe93Mv2u"  JFE - 8/22/2014
         
            //Login = "6D2kYx9bQH",
            //TransactionKey = "97Z9MRn69cnU77yx"

            //Login = "7CA72bjH",
            //TransactionKey = "48rESS6Nn233b9Rs"
        };

        public IPaymentRequest CreateFrom(Registration from)
        {
            return CreateFrom(from, true);
        }

        public IPaymentRequest CreateFrom(Registration from, bool asTest)
        {
            var cc = from.CreditCardInfo;
            var payee = from.Registrant;

            var firstName = payee.FirstName;
            var lastName = payee.LastName;

            return new PaymentRequest(
                CommerceAccount.Login,
                CommerceAccount.TransactionKey,
                asTest,
                firstName,
                lastName,
                payee.Address,
                payee.City,
                payee.State,
                payee.Zip,
                "USA",
                "Pastor's Cup Registration",
                cc.CardNumber,
                GetCardExpiration(cc.ExpirationMonth, cc.ExpirationYear),
                cc.VerificationNumber,
                from.TotalCharge,
                "Pastors Cup");
        }

        public IPaymentRequest CreateFrom(Purchase from, bool asTest)
        {
            var cc = from.CreditCardInfo;
            var payee = from.Customer;
            var firstName = payee.FirstName;
            var lastName = payee.LastName;

            return new PaymentRequest(
                CommerceAccount.Login,
                CommerceAccount.TransactionKey,
                asTest,
                firstName,
                lastName,
                payee.Address,
                payee.City,
                payee.State,
                payee.Zip,
                "USA",
                "Pastor's Cup Purchase",
                cc.CardNumber,
                GetCardExpiration(cc.ExpirationMonth, cc.ExpirationYear),
                cc.VerificationNumber,
                from.TotalCharge,
                "Pastors Cup");
        }

        public IPaymentRequest CreateFrom(Sponsorship from, bool asTest)
        {
            var cc = from.CreditCardInfo;
            var payee = from.CompanyContactInfo;

            var firstName = from.CompanyName;
            var lastName = String.Empty;

            return new PaymentRequest(
                CommerceAccount.Login,
                CommerceAccount.TransactionKey,
                asTest,
                firstName,
                lastName,
                payee.Address,
                payee.City,
                payee.State,
                payee.Zip,
                "USA",
                "Pastor's Cup Sponsorship: " + from.Level.Type,
                cc.CardNumber,
                GetCardExpiration(cc.ExpirationMonth, cc.ExpirationYear),
                cc.VerificationNumber,
                from.TotalCharge,
                "Pastors Cup");
        }

        private static string GetCardExpiration(int month, int year)
        {
            return string.Format("{0}/{1}",
                                 month.ToString("00"),
                                 year.ToString("00"));
        }
    }
}
