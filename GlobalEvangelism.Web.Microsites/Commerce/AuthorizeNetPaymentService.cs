using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using System.Text;

namespace CommerceIntegration
{
    public class AuthorizeNetPaymentService : IPaymentService
    {
        public static readonly string LiveAuthorizeNetUrl = ConfigurationManager.AppSettings["LiveAuthorizeNetUrl"];    

        private readonly string AuthorizeNetVersion = "3.1";

        public bool TestTransaction()
        {
            //public static readonly Company ChurchAccount = new Company(1, "CHURCH Account", CommerceAccountTypes.AuthorizeDotNet, "22r8w9LKq", "2H2K3v8cVe93Mv2u", "",
            //  "Cornerstone Church", "18755 Stone Oak Parkway", "San Antonio, TX 78258", "210-490-1600", "www.sacornerstone.org", GetChurchDonationMessage(), GetChurchLogo(), "", ReportDirectory.OnNetwork);

            IPaymentRequest req = new PaymentRequest("22r8w9LKq","2H2K3v8cVe93Mv2u",true,"Dave","Buster","237 W HWY 1604", "San Antonio","Texas","78258","USA","Test","4444333322221111","01/17","333",1.44M,"555");
            IPaymentResponse resp = Process(req);
            if (resp != null)
            {
                return true;
            }
            return false;

        }

        public IPaymentResponse Process(IPaymentRequest request)
        {
            try
            {
                //Pure Test Server
                byte[] responseBody = ExecuteWebRequest(LiveAuthorizeNetUrl,
                    CreatePayload(request, request.IsTest));

                string[] responseItems = Encoding.ASCII.GetString(responseBody).Split(',');

                if (responseItems[0].Trim('|') == "1")
                {
                    return new PaymentResponse(true, string.Empty,
                                                   responseItems[4].Trim('|'),
                                                   responseItems[6].Trim('|'));
                }
                // Error!
                    
                string errorMessage =  string.Format("{0} ({1})", 
                                                     responseItems[3].Trim('|'),
                                                     responseItems[2].Trim('|'));

                if (responseItems[2].Trim('|') == "44")
                {
                    // CCV transaction decline
                    errorMessage += "Our Card Code Verification (CCV) returned the following error: ";

                    switch (responseItems[38].Trim('|'))
                    {
                        case "N":
                            errorMessage += "Card Code does not match.";
                            break;

                        case "P":
                            errorMessage += "Card Code was not processed.";
                            break;

                        case "S":
                            errorMessage += "Card Code should be on card but was not indicated.";
                            break;

                        case "U":
                            errorMessage += "Issuer was not certified for Card Code.";
                            break;
                    }
                }

                if (responseItems[2].Trim('|') == "45")
                {
                    if (errorMessage.Length > 1)
                        errorMessage += "<br />n";

                    // AVS transaction decline
                    errorMessage += "Our Address Verification System (AVS) returned the following error: ";

                    switch (responseItems[5].Trim('|'))
                    {
                        case "A":
                            errorMessage += " the zip code entered does not match the billing address.";
                            break;

                        case "B":
                            errorMessage += " no information was provided for the AVS check.";
                            break;

                        case "E":
                            errorMessage += " a general error occurred in the AVS system.";
                            break;

                        case "G":
                            errorMessage += " the credit card was issued by a non-US bank.";
                            break;

                        case "N":
                            errorMessage += " neither the entered street address nor zip code matches the billing address.";
                            break;

                        case "P":
                            errorMessage += " AVS is not applicable for this transaction.";
                            break;

                        case "R":
                            errorMessage += " please retry the transaction; the AVS system was unavailable or timed out.";
                            break;

                        case "S":
                            errorMessage += " the AVS service is not supported by your credit card issuer.";
                            break;

                        case "U":
                            errorMessage += " address information is unavailable for the credit card.";
                            break;

                        case "W":
                            errorMessage += " the 9 digit zip code matches, but the street address does not.";
                            break;

                        case "Z":
                            errorMessage += " the zip code matches, but the address does not.";
                            break;
                    }
                }

                return new PaymentResponse(false, errorMessage, string.Empty, string.Empty);
            }
            catch (Exception ex)
            {
                return new PaymentResponse(false,
                    string.Format("Processing ERROR: {0}", ex.Message), string.Empty, string.Empty);
            }
        }

        private static byte[] ExecuteWebRequest(string baseAddress, NameValueCollection data)
        {
            var webClient = new WebClient {BaseAddress = baseAddress};
            return webClient.UploadValues(webClient.BaseAddress, "POST", data);
        }

        private NameValueCollection CreatePayload(IPaymentRequest request, bool isTest)
        {
            NameValueCollection data = new NameValueCollection(30);

            //objInf.Add("x_password", AuthNetPassword);
            data.Add("x_version", AuthorizeNetVersion);
            data.Add("x_login", request.LoginID);
            data.Add("x_tran_key", request.TransactionKey);
            data.Add("x_test_request", isTest ? "True" : "False"); 
            
            data.Add("x_delim_data", "True");
            data.Add("x_relay_response", "False");
            data.Add("x_delim_char", ",");
            data.Add("x_encap_char", "|");

            data.Add("x_first_name", request.FirstName);
            data.Add("x_last_name", request.LastName);
            data.Add("x_address", request.Address);
            data.Add("x_city", request.City);
            data.Add("x_state", request.State);
            data.Add("x_zip", request.Zip);
            data.Add("x_country", request.Country);
            data.Add("x_description", request.DescriptionOfCharge);
            if (request.InvoiceNumber != null)
            {   
                data.Add("x_invoice_num", request.InvoiceNumber);
            }
            // Card Details
            data.Add("x_card_num", request.CardNumber);
            data.Add("x_exp_date", request.CardExpDate);

            if (!string.IsNullOrEmpty(request.CardCCV) &&
                !request.CardCCV.Trim().Equals(String.Empty))
                data.Add("x_card_code", request.CardCCV);

            data.Add("x_method", "CC");
            data.Add("x_type", "AUTH_CAPTURE");
            data.Add("x_amount", request.Amount.ToString());

            // Currency setting. Check the guide for other supported currencies
            data.Add("x_currency_code", "USD");

            return data;
        }

    }
}
