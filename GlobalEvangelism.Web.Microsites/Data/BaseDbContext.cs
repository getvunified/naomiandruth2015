﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace GlobalEvangelism.Web.Microsites
{
	public class BaseDbContext<TContext> : DbContext where TContext : DbContext, new()
	{
		public static void Initialize()
		{
			using (var context = new TContext())
			{
				//context.Database.Initialize(force:true);
			}
		}
	}
}