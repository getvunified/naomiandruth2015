﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using GlobalEvangelism.Web.Microsites;
using GlobalEvangelism.Web.Microsites.Models;
using Newtonsoft.Json;


namespace Content.Api
{
	public class MicrositeContextInitializer : IDatabaseInitializer<MicrositeContext> //  - manual script migrations in effect.
	{
        protected void Seed(MicrositeContext context)
	    {

	        try
	        {
                    var availableevents = new List<Event>();

                    var url = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/Event-WOENAR.json";
                    using (StreamReader r = new StreamReader(url))
                    {
                        string json = r.ReadToEnd();
                        availableevents = JsonConvert.DeserializeObject<List<Event>>(json);
                    }

                    using (MicrositeContext db = new MicrositeContext())
                    {
                        foreach (Event eve in availableevents)
                        {
                            db.Events.AddOrUpdate(eve);
                            db.SaveChanges();
                        }                           
                    }

                    var eventsessions = new List<EventSession>();
            
                    url = AppDomain.CurrentDomain.BaseDirectory + "/App_Data/EventSessions-WOENAR.json";
                    using (StreamReader r = new StreamReader(url))
                    {
                        string json = r.ReadToEnd();
                        eventsessions = JsonConvert.DeserializeObject<List<EventSession>>(json);
                    }

                    using (MicrositeContext db = new MicrositeContext())
                    {
                        foreach (EventSession evesess in eventsessions)
                        {
                            db.EventSessions.AddOrUpdate(evesess);
                            db.SaveChanges();
                        }
                    }
	        }
	        catch (Exception ex)
	        {            
	            throw ex;
	        }
           
	    }

	    
        public void InitializeDatabase(MicrositeContext context)
        {
            Seed(context);
        }

    }
}