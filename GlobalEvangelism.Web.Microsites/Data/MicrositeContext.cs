﻿using System.Data;
using System.Data.Entity;
using GlobalEvangelism.Web.Microsites.Models;

namespace GlobalEvangelism.Web.Microsites
{
    public class MicrositeContext : BaseDbContext<MicrositeContext>
    {
        static MicrositeContext()
        {            
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<MicrositeContext>());
            Database.SetInitializer<MicrositeContext>(null);
        }

        public void DeleteAll<T>(DbContext context)
      where T : class
        {
            foreach (var p in context.Set<T>())
            {
                context.Entry(p).State = EntityState.Deleted;
            }
        }

        public MicrositeContext()
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.AutoDetectChangesEnabled = true;
        }

        public DbSet<Registration> Registrations { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventSession> EventSessions { get; set; }


        public void SaveOrUpdate<T>(T entity) where T : class
        {
            // Need to determine if attaching for update or adding
            bool entitytester = true;

            //this.Set<T>().SqlQuery()

            if (this.Entry(entity).State == EntityState.Unchanged)
            {
                this.Set<T>().Attach(entity);
            }
            // Add the entity if it's new, otherwise let EF do itz majik update
            else
            {
                if (this.Entry(entity).State == EntityState.Detached)
                {
                    this.Set<T>().Add(entity);
                }
            }
            this.SaveChanges();
        }

	}
}