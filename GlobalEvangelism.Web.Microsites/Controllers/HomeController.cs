﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using AuthorizeNet;
using GlobalEvangelism.Web.Microsites.Models;
using Jhm.Code;
using Microsoft.Ajax.Utilities;
using CommerceIntegration;

namespace GlobalEvangelism.Web.Microsites.Controllers
{
    public class HomeController : Controller
    {
        MicrositeContext db = new MicrositeContext();

        public static string sitemode;
        public static string sitename;
        public static string username;
        public static string eventid;
        public static Event eventinfo;
        public static List<EventSession> eventsessioninfo; 

        protected string ErrorMessageScript { get; set; }
        protected Registration Registration { get; set; }
        private readonly EmailService emailService = new EmailService();

        public ActionResult Index(string returnUrl)
        {
            ViewBag.ShowAbout = true;
            ViewBag.SiteHasLogin = false;

            if (Request.QueryString != null)
            {               
                if (Request.QueryString["sitename"] != null)
                {
                    sitename = Request.QueryString["sitename"].ToUpper().Trim();                  
                }

                if (Request.QueryString["sitemode"] != null)
                {
                    sitemode = Request.QueryString["sitemode"].ToUpper().Trim();
                    // Find the sitemode and preset accordingly, for now GlobalEvangelism.Web.Microsites only allow about and registration
                    if (sitemode != "ABOUT")
                    {
                        ViewBag.ShowAbout = false;
                        ViewBag.ShowRegister = true;
                    }
                }

                if (Request.QueryString["eventid"] != null)
                {
                    eventid = Request.QueryString["eventid"].ToUpper().Trim();
                    
                }        

                if (Request.QueryString["username"] != null)
                {
                    username = Request.QueryString["username"].ToUpper().Trim();
                }
            }
        
            sitemode = sitemode?? ConfigurationSettings.AppSettings["default_sitemode"];
            sitename = sitename ?? ConfigurationSettings.AppSettings["default_sitename"];
            eventid = eventid ?? ConfigurationSettings.AppSettings["default_eventid"];

            Session["sitename"] = sitename;
            Session["eventid"] = eventid;

            eventinfo = GetEvent(eventid);
            if (eventinfo == null)
            {
                // for now show nothing on error   - - -needs to return 404 or something
                return null;
            }

            ViewBag.SiteHasLogin = eventinfo.SiteRequiresLogin;
            
            //ViewBag.HeaderHTML = "<div><img src='../Images/Content/0-NR-goldcrown&leaf-psd.png' class='headerImg' /><div class='headerText'><img src='../Images/Content/0-Naomi & Ruth head-psd.png' alt='nar' class='subHeaderText' /><h3>Discipleship School - 2014/2015 Session</h3><h2>\"Queen Esther, Making Right Choices;<br/>Becoming a world Changer!\"</h2></div></div>";
            //if (eventinfo.HeaderHTML.IsNullOrWhiteSpace())
            //{
                ViewBag.HeaderHTML =
                    "<div style=\"font-size: 20px;width: 100%;text-align: left;\"><span><a href=\"http://www.jhm.org\"><img src=\"http://media.jhm.info/Images/www/img/FootLogoCC.png\" style=\"height:75px\" class=\"hidden-xs\">Cornerstone Church</a> and <a href=\"http://www.sacornerstone.org/women-of-esther\">Women of Esther</a> presents</span></div><div class=\"col-sm-3 hidden-xs\"><img src=\"../Images/Content/0-NR-goldcrown&amp;leaf-psd.png\" style=\"width: 100%;height: auto;\"></div><div class=\"col-sm-9\"><img src=\"../Images/Content/0-Naomi &amp; Ruth head-psd.png\" style=\"width: 100%;height: auto;\" alt=\"nar\"></div><div class=\"col-sm-9\"><h3>Discipleship School - 2015/2016 Session</h3></div><div class=\"col-md-9\"><h2>\"HANNAH'S SONG; WHAT LOVE LOOKS LIKE!\"<br> 1 Samuel 1; 2:1,21</h2></div>";
            //}

            // Check for registration cookie eventually?  TODO 
            ViewBag.AboutPageModel = GetAboutPage(eventinfo);
            ViewBag.RegisterModel = GetRegistration(eventinfo, username);
            ViewBag.ReturnUrl = returnUrl; // pass throught query string stuff  
            return View();
        }


        /// <summary>
        /// Pull the site/event specified in the querystring, and preset the registration form parameters
        /// </summary>
        /// <param name="sitename"></param>
        /// <param name="eventid"></param>
        /// <returns></returns>
        private Event GetEvent(string eventid)
        {
            var thisevent = db.Set<Event>()
                .Include(x => x.Sessions)
                .SingleOrDefault(e => e.EventID.ToUpper() == eventid.ToUpper());

            if (thisevent == null)
            {
                return null;
            }

            // now check event dates to preset the form:
            DateTime rightnow = DateTime.UtcNow; 
            
            // PREREGISTRATION PERIOD
             if (thisevent.PreRegistrationStartDate <= rightnow & thisevent.PreRegistrationEndDate >= rightnow)
            {
                thisevent.PreRegistrationChecked = true;
            }
            if (thisevent.PreRegistrationEndDate >= rightnow & thisevent.FullRegistrationStartDate <= rightnow)
            {
                thisevent.PreRegistrationChecked = false;
                thisevent.FullRegistrationChecked = false;
            }
           
            // REGISTRATION PERIOD
            if (thisevent.FullRegistrationStartDate <= rightnow & thisevent.FullRegistrationEndDate >= rightnow)
            {
                thisevent.FullRegistrationChecked = true;
            }

            // EVENT SESSIONS LOADED HERE, filtered by start date and SeatsAvailable
           // thisevent.Sessions = GetEventSessions(eventid);

            return thisevent;
        }

        /// <summary>
        /// Pull all the session for this event, and filter result for datetime(UTC)
        /// </summary>
        /// <param name="eventid"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        private List<EventSession> GetEventSessions(string eventid = "", string username = "")
        {
            try
            {                           
                DateTime rightnow = DateTime.UtcNow;
                var availableeventsessions = db.EventSessions.Where(d => d.SeatsAvailable & d.EndDate >= rightnow);
                return availableeventsessions.ToList();
            }
            catch (Exception ex)
            {
                
                return null;
            }    
        }

        private EventSession GetEventSession(string eventsessionid, string eventid = "")
        {            
            try
            {
                return db.EventSessions.FirstOrDefault(e => e.Id == eventsessionid);
            }
            catch (Exception)
            {
                return null; // allow the default empty event sessions to pass
            }
        }


        /// <summary>
        /// Populate the about page content from the database
        /// </summary>
        /// <returns></returns>
        private AboutPageModel GetAboutPage(Event currentEvent)
        {
            var aboutpage = new AboutPageModel();
            aboutpage.AboutPageItemId = 1;
            aboutpage.TitleImage = null;
            aboutpage.HeaderHTML = currentEvent.HeaderHTML;
            aboutpage.InnerHTML = currentEvent.InnerHTML;
            aboutpage.FooterHTML = currentEvent.FooterHTML;
            aboutpage.Event = currentEvent;
            return aboutpage;
        }


        /// <summary>
        /// Check to see if a username/emailaddress has registered already for this event
        /// </summary>
        /// <param name="eventinfo"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        private RegisterModel GetRegistration(Event eventinfo, string username)
        {
            var user = new RegisterModel();
            user.Registration = new Registration();
            if (username == string.Empty)
            {
                user.UserName = "";
                user.Registration.Email = "";
            }
            // Get these from persistence in 1.0 version, for now static
            user.Registration.Event = eventinfo;
           // user.Registration.Event.EventSessions = GetEventSessions(eventinfo.EventID, username);
            user.Registration.ContactPreferenceOptions = ConfigurationSettings.AppSettings["default_contactpreference"];
            ViewBag.SubHeader = "<div style='text-align:center;color: #866228;'><h4>REGISTRATION INFORMATION</h4><input type='button' style='margin: 20px auto; display: block;' onclick='ShowPanel('About');' value='Back to About' /></div>";

            //we need to populate the default AmountDue if we're making Pre or Full Registration default true
            if (user.Registration.Event.FullRegistrationChecked)
            {
                user.Registration.AmountDue = user.Registration.Event.FullRegistrationPrice;
            }
            else if (user.Registration.Event.PreRegistrationChecked)
            {
                user.Registration.AmountDue = user.Registration.Event.PreRegistrationPrice;
            }

            return user;
        }


        /// <summary>
        /// Validate Registration and present Summary for review or return back to the Reg page
        /// </summary>
        /// <param name="pageModel"></param>
        /// <param name="form"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public ActionResult ReviewRegistration(RegisterModel pageModel, string returnUrl ="")
        {
            List<EventSession> selectedSessions = new List<EventSession>();

            if (pageModel.Registration.Event.PreRegistrationChecked)
            {
                selectedSessions = GetEventSessions(pageModel.Registration.Event.EventID);
            }
            else if (pageModel.Registration.Event.FullRegistrationChecked)
            {
                selectedSessions = GetEventSessions(pageModel.Registration.Event.EventID);
            }
            else
            {
                selectedSessions = (from s in pageModel.Registration.EventSessionsConcat.Split('|') select GetEventSession(s)).ToList();
            }

            var eventinfo = GetEvent(pageModel.Registration.Event.EventID);
            pageModel.Registration.Event.FullRegistrationDescription = eventinfo.FullRegistrationDescription;
            pageModel.Registration.Event.PreRegistrationDescription = eventinfo.PreRegistrationDescription;

            pageModel.Registration.Initialize(); 
            Session["PreviewRegistration"] = pageModel;
            Session["SelectedSessions"] = selectedSessions;
            ViewBag.SelectedSessions = selectedSessions;
            ViewBag.ReturnUrl = returnUrl;
            // cleanup registration for summary page - SPA
            
            return PartialView("_RegistrationSummary",pageModel);
        }


        /// <summary>
        /// Finalize Purchase and present finaly summary with RegistrationCode
        /// </summary>
        /// <param name="pageModel"></param>
        /// <returns></returns>
        public ActionResult SubmitRegistration(RegisterModel pageModel, FormCollection form)
        {
            // Set up all the db stuff, note Form Posts are empty at this point.
            try
            {
                if (Session["PreviewRegistration"] == null )
                {
                    throw new Exception("An error has occured in registration.");
                }

                RegisterModel registerpagemodel = (RegisterModel)Session["PreviewRegistration"];
                Registration registration = registerpagemodel.Registration;
                registration.EventID = registration.Event.EventID;

                // sanitze the card info and do a bunch of model formatting for persistence
                registration = PrepareRegistration(registration);

                // Save the registration info
                var getRegistrationResult = ProcessRegistration(registration);
                if (getRegistrationResult != null)
                {
                    return Json(new
                    {
                        errorCatagory = "Registration",
                        errorMessage = "An unknown error has occurred. Please contact Special Events at 210-499-1669."
                    }, JsonRequestBehavior.AllowGet);
                }

                // Submit Payment
                IPaymentResponse submitPaymentResult = SubmitPayment(registration);
                if(submitPaymentResult != null && !submitPaymentResult.IsAuthorized)
                {
                    return Json(new
                    {
                        errorCatagory = "Payment",
                        errorMessage = submitPaymentResult.ReasonText
                    }, JsonRequestBehavior.AllowGet);
                }

                registration.AmountPaid = registration.AmountDue;                  
                registration.AmountDue = 0.00M;
                registration.IsPaid = true;
                registration.EventID = registration.Event.EventID;
                registration.TransactionID = submitPaymentResult.TransactionId;
                int cardlen = registration.CreditCardNumber.Length;
                if (cardlen > 4)
                {
                    registration.CreditCardNumber = registration.CreditCardNumber.Substring(cardlen - 4, 4);
                }

                // Assign a Registration Code
                registration.RegistrationCode = Guid.NewGuid().ToString("N").Substring(4, 6).ToUpper();

                // Now update registration
                var getRegistrationUpdateResult = ProcessRegistration(registration);
              //
                if (getRegistrationUpdateResult != null)
                {
                    return Json(new
                    {
                        errorCatagory = "Registration",
                        errorMessage = "An unknown error has occurred updating your registration. Your payment has been processed but we were unable to save your registration. Please contact Special Events at 210-499-1669, and provide them with the following transaction number: " + submitPaymentResult.TransactionId
                    }, JsonRequestBehavior.AllowGet);
                }

                string msg = string.Format("<p>Thank you for your registration purchase. Please <a onclick='window.print();'>print this page</a> for your records and bring it with you to the Event. </P> <p><H3>YOUR REGISTRATION CODE IS: {0}</H3></p>", registration.RegistrationCode);

                if (SendReceiptEmail(registration, submitPaymentResult))
                {
                    msg += String.Format(" A confirmation email has been sent to {0}.", registration.Email);
                }
 
                return Json(new
                {
                    successMessage = msg,
                    registrationCode = registration.RegistrationCode
                });
            }
            catch (Exception ex)
            {
                ViewBag.Registermodel = new RegisterModel();
                ViewBag.Registermodel.Registration.EventSessions = GetEventSessions();
                ViewBag.SuccessMessage = String.Empty;
                ViewBag.ErrorMessage = "Unable to Process Registrations at this time. Please try again later.";
                return PartialView("_Register", ViewBag.Registermodel);
            }
     
        }


        private IPaymentResponse SubmitPayment(Registration registration)
        {
            AuthorizeNetPaymentService service = new AuthorizeNetPaymentService();
            bool isTest = ConfigurationManager.AppSettings["SubmitTransactionsAsTest"] == "true";
            string loginID = ConfigurationManager.AppSettings["AuthorizeNETloginKey"];
            string transactionKey = ConfigurationManager.AppSettings["AuthorizeNETtransactionKey"];

            if (registration.PaymentType.ToUpper() == "CreditCard".ToUpper())
            {
                
                IPaymentRequest req = new PaymentRequest(loginID, transactionKey, isTest, registration.CreditCardName.Split('|')[0], registration.CreditCardName.Split('|')[1], null, null, null, null, null, "Event Registration", registration.CreditCardNumber, registration.CreditCardExpirationDate, registration.CreditCardCID, registration.AmountDue, "555");
                return service.Process(req);
            }
            else if (registration.PaymentType.ToUpper() == "Check".ToUpper())
            {
                if (isTest)
                {
                    return new PaymentResponse(true,"Test Check","Approved123","0");
                }

                // NOTE: For eChecks to process properly, must use production API URL and testMOde=false. There is NO testMode for checks! 9/9/2014
                Gateway gateway = new Gateway(loginID, transactionKey, false);

                //Submit check payment
                EcheckRequest echeck = new EcheckRequest(registration.AmountDue,registration.CheckRoutingNumber,registration.CheckAccountNumber,BankAccountType.Checking,registration.CheckBankName, String.Format("{0} {1}",registration.FirstName, registration.LastName), "10000" );
                if (registration.IsAGift)
                {
                    echeck = new EcheckRequest(registration.AmountDue, registration.CheckRoutingNumber, registration.CheckAccountNumber, BankAccountType.Checking, registration.CheckBankName, registration.GiftInfo, "10000");
                }
                IGatewayResponse resp = gateway.Send(echeck);                              
                IPaymentResponse returnResponse = new PaymentResponse(resp.Approved, resp.Message, resp.AuthorizationCode, resp.TransactionID);
                return returnResponse;
            }
            return null;
        }

        private Registration PrepareRegistration(Registration registration)
        {
            registration.Initialize();

            registration.SelectedEventSessions = (List<EventSession>)Session["SelectedSessions"];
            if (registration.PaymentType.ToUpper() == "CHECK")
            {   // will satisfy EF6 db validation, but will intentionally fail ISNUMERIC tests
                registration.CreditCardType = "NONE";
                registration.CreditCardNumber = "*";
                registration.CreditCardCID = "*";
                registration.CreditCardName = "*";
                registration.CreditCardExpirationDate = DateTime.UtcNow.ToLongDateString();
            }
            else
            {   // will intentionally fail ISNUMERIC tests
                registration.CheckAccountNumber = "*";
                registration.CheckRoutingNumber = "*";                             
            }

            if (!registration.IsAGift)
            {
                registration.GiftInfo = "*";
                registration.GiftEmail = "*";
            }

            if (string.IsNullOrWhiteSpace(registration.Email))
                registration.Email = "noreply@jhm.org";
            if (string.IsNullOrWhiteSpace(registration.WorkPhone))
                registration.WorkPhone = "*";
            if (string.IsNullOrWhiteSpace(registration.HomePhone))
                registration.HomePhone = "*";
            if (string.IsNullOrWhiteSpace(registration.MobilePhone))
                registration.MobilePhone = "*";

            registration.EventID = registration.Event.EventID;

            return registration;
        }

        private ValidationResult ProcessRegistration(Registration registration)
        {
            try
            {
                db.Registrations.AddOrUpdate(registration);
                db.SaveChanges();
                return null;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
              //  throw e;
            }

            catch (Exception ex)
            {
                var validation = new ValidationResult(ex.Message);
                return validation;
            }

            return new ValidationResult("");

        }

        //private ValidationResult UpdateRegistration(Registration registration)
        //{
        //    try
        //    {
        //        Models.Registration foundReg = db.Registrations.FirstOrDefault(r => r.EventID == registration.EventID
        //            & r.FirstName == registration.FirstName
        //            & r.LastName == registration.LastName
        //            & (r.Email == registration.Email | r.MobilePhone == registration.MobilePhone | r.HomePhone == registration.HomePhone));
                
        //        if (foundReg == null)
        //        {
        //            throw new Exception("original registration did not save.");
        //        }
        //    //    db.Registrations.Attach(foundReg);
        //    //    ObjectStateEntry entry =  ObjectStateManager.GetObjectStateEntry(foundReg);
        //        foundReg.AmountDue = registration.AmountDue;
        //        foundReg.AmountPaid = registration.AmountPaid;
        //        foundReg.IsPaid = registration.IsPaid;
        //        foundReg.TransactionID = registration.TransactionID;
        //        foundReg.RegistrationCode = registration.RegistrationCode;              

        //         db.SaveOrUpdate(foundReg);
        //         return null;
                
        //    }
        //    catch (DbEntityValidationException e)
        //    {
        //        foreach (var eve in e.EntityValidationErrors)
        //        {
        //            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
        //                eve.Entry.Entity.GetType().Name, eve.Entry.State);
        //            foreach (var ve in eve.ValidationErrors)
        //            {
        //                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
        //                    ve.PropertyName, ve.ErrorMessage);
        //            }
        //        }
        //        //  throw e;
        //    }

        //    catch (Exception ex)
        //    {
        //        var validation = new ValidationResult(ex.Message);
        //        return validation;
        //    }

        //    return new ValidationResult("");
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool SendReceiptEmail(Registration registration, IPaymentResponse response = null)
        {
            try
            {
                var emailMessage = emailService.CreateClientEmail(registration, response, AppDomain.CurrentDomain.BaseDirectory + "/EmailTemplates/registration_client.htm");
                if (registration.ContactPreference.ToUpper() == "EMAIL" && !registration.Email.IsNullOrWhiteSpace())
                {
                    emailService.SendEmail(emailMessage, registration.Email);
                }
                if (registration.IsAGift)
                {
                    emailService.SendEmail(emailMessage, registration.GiftEmail);
                }
                emailService.SendEmail(emailMessage, ConfigurationSettings.AppSettings["default_registration_email_cc"]);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


    }
}