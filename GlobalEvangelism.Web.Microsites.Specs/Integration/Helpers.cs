﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Machine.Specifications;
using StructureMap.AutoMocking;

namespace GlobalEvangelism.Web.Microsites.Specs.Integration
{
    class Helpers
    {
        public abstract class With<TSubject>
      where TSubject : class
        {
            Establish context = () =>
            {
                AutoMocker = new RhinoAutoMocker<TSubject>();
            };

            public static TDependency Get<TDependency>()
                where TDependency : class
            {
                return AutoMocker.Get<TDependency>();
            }

            public static RhinoAutoMocker<TSubject> AutoMocker { get; private set; }
            public static TSubject Subject { get { return AutoMocker.ClassUnderTest; } }
        }
    }
}
