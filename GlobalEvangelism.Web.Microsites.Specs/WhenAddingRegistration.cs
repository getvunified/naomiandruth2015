﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlobalEvangelism.Web.Microsites;
using GlobalEvangelism.Web.Microsites.Models;
using Machine.Specifications;
using System.Threading.Tasks;
using Machine.Specifications.Model;

namespace GlobalEvangelism.Web.Microsites.Specs
{
    public class WhenAddingRegistration
    {
            [Subject("NewRegistration")]
            public class when_saving_a_new_registration
            {
                private static RegisterModel registration;

                private static EventSession eventsession;

                public static Registration newreg;

                private Establish context = () =>
                {
                    var newreg = new Registration();
                    newreg.Initialize();
                    newreg.Event = new Event();
                    newreg.Event.Sessions = new List<EventSession>();
                    newreg.FirstName = "Firtst";
                    newreg.LastName = "Lasdts";
                    newreg.Email = "email@email";
                };

                Because of = () =>
                {
                    using (MicrositeContext db = new MicrositeContext())
                    {
                        db.Registrations.Add(newreg);
                        db.SaveChanges();
                    }
                };

                It should_have_saved_the_registration = () =>
                {
                    using (MicrositeContext db = new MicrositeContext())
                    {    var result = db.Registrations.SingleOrDefault(i=> i.Id == newreg.Id);
                         result.Email.ShouldEqual(newreg.Email);
                        // Cleanup
                    }
                };

            }
        }

}
